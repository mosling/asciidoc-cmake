# This function generate some version informations based on the git branch
# (using gitlabs CI_COMMIT_BRANCH or using git rev-parse) and the git describe result.
#
# Set the following variables in the parent scope (caller)
# GIT_VERSION          : the version of the document
# GIT_ARTIFACT_VERSION : can be used in filenames <major>.<minor>
# GIT_STATE            : derived from branch (main: Release, release: Release Candidate, other: Draft)
# GIT_DOCDATE          : the commit date or if local changes exsits the current timestamp
# BUILD_TIMESTAMP      : timestamp depends on branch if with time or not
FUNCTION(VERSION_FROM_GIT)

    find_package(Git)
    if (NOT Git_FOUND)
        message(FATAL_ERROR "git executable not found")
        return()
    endif()

    # the current timestamp
    string(TIMESTAMP TS "%Y-%m-%d" UTC)

    # get last tag, difference and git commit hash
    execute_process(
        COMMAND ${GIT_EXECUTABLE} describe --tags --long
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE GIT_REPO_VERSION
    )
    string(REGEX REPLACE "\n$" "" GIT_REPO_VERSION "${GIT_REPO_VERSION}")

    # get the date of this commit
    execute_process(
        COMMAND ${GIT_EXECUTABLE} show -s --format=format:%cs ${GIT_REPO_VERSION}
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE GIT_COMMIT_DATE
    )

    # this environment variable is set if running in gitlab ci build
    if(DEFINED ENV{CI_COMMIT_BRANCH})
        set(GIT_REPO_BRANCH $ENV{CI_COMMIT_BRANCH})
    else()
        execute_process(
            COMMAND ${GIT_EXECUTABLE} rev-parse --abbrev-ref HEAD
            WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
            OUTPUT_VARIABLE GIT_REPO_BRANCH
        )
    endif()
    string(REGEX REPLACE "\n$" "" GIT_REPO_BRANCH "${GIT_REPO_BRANCH}")

    # check if all changes commited
    set(LOCAL_CHANGES "")
    execute_process(
        COMMAND ${GIT_EXECUTABLE} diff --exit-code
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_QUIET
        RESULT_VARIABLE STATUS_RET
    )

    if(STATUS_RET EQUAL "1")
        message( INFO " local changes detected -- use current date for document")
        set( LOCAL_CHANGES " -- with local changes")
        # in this case the GIT_COMMIT_DATE is set to current date
        set(GIT_COMMIT_DATE ${TS})
    endif()

    string(STRIP "${GIT_REPO_VERSION}" GIT_REPO_VERSION )
    string(STRIP "${GIT_REPO_BRANCH}" GIT_REPO_BRANCH )
    string(REPLACE "/" "-" GIT_REPO_BRANCH "${GIT_REPO_BRANCH}")

    # split x.y[.z]-build-ghash into ("x.y[.z]", "b", "ghash")
    string(REPLACE "-" ";" LIST_GIT_REPO_VERSION ${GIT_REPO_VERSION})

    # extract version information from the first part
    list(GET LIST_GIT_REPO_VERSION 0 MAJOR_MINOR)
    string(REPLACE "." ";" LIST_MAJOR_MINOR ${MAJOR_MINOR})
    list(GET LIST_MAJOR_MINOR 0 GIT_MAJOR)
    list(GET LIST_MAJOR_MINOR 1 GIT_MINOR)

    # get build-number and hash if available
    list(LENGTH LIST_GIT_REPO_VERSION REPO_LENGTH)
    if ( ${REPO_LENGTH} GREATER 1)
        # this conditions matches all non direct tagged versions
        list(GET LIST_GIT_REPO_VERSION 1 GIT_BUILD_NUMBER)
        list(GET LIST_GIT_REPO_VERSION 2 GIT_HASH_PREFIXED)
        STRING(REGEX REPLACE "^g" "" GIT_HASH ${GIT_HASH_PREFIXED})
    else()
        # this condition is reached for direct tag commits
        set(GIT_BUILD_NUMBER 0)
        set(GIT_HASH "")
    endif()

    string(FIND GIT_REPO_BRANCH "release" GIT_REPO_RELEASE)

    if ( (${GIT_REPO_BRANCH} STREQUAL "master") OR ((${GIT_REPO_BRANCH} STREQUAL "main")) OR (${GIT_REPO_BRANCH} STREQUAL "HEAD") )
        # variant 1 for master and direct tag builds
        set(GIT_VERSION "${MAJOR_MINOR}")
        set(GIT_STATE "Release")
    else()
        # variant 2 for all other builds (develop/feature/...)
        set(GIT_VERSION "${MAJOR_MINOR}-${GIT_BUILD_NUMBER} (Git: ${GIT_HASH}${LOCAL_CHANGES})")
        if (${GIT_REPO_RELEASE} EQUAL 0)
            set(GIT_STATE "Release Canditate")
        else()
            set(GIT_STATE "Draft")
        endif()
    endif()

    set(GIT_ARTIFACT_VERSION "${GIT_MAJOR}.${GIT_MINOR}")

    # set return values in parent scope
    set(GIT_VERSION ${GIT_VERSION} PARENT_SCOPE)
    set(GIT_ARTIFACT_VERSION ${GIT_ARTIFACT_VERSION} PARENT_SCOPE)
    set(GIT_STATE ${GIT_STATE} PARENT_SCOPE)
    set(GIT_DOCDATE ${GIT_COMMIT_DATE} PARENT_SCOPE)
    set(BUILD_TIMESTAMP ${TS} PARENT_SCOPE)

ENDFUNCTION()


FUNCTION(HISTORY_FROM_GIT)

    find_package(Git)
    if (NOT Git_FOUND)
        message(FATAL_ERROR "git executable not found")
        return()
    endif()

    set(HISTORY "")

    execute_process(
        COMMAND ${GIT_EXECUTABLE} for-each-ref refs/tags "--format=%(objecttype) %(refname) %(subject)"
        WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
        OUTPUT_VARIABLE TAG_LIST_BLOCK
    )

    string(REPLACE "\n" ";" TAG_LIST ${TAG_LIST_BLOCK})
    FOREACH(TAG_LINE IN ITEMS ${TAG_LIST})
        string(REPLACE " " ";" TAG_PARAMETER ${TAG_LINE})
        list(GET TAG_PARAMETER 0 TAG_KIND)
        list(GET TAG_PARAMETER 1 TAG_NAME)
        if ("${TAG_KIND}" STREQUAL "tag")
            message(INFO "           add : ${TAG_LINE}")
            execute_process(
                COMMAND ${GIT_EXECUTABLE} for-each-ref ${TAG_NAME} "--format=|%(refname:short)|%(creatordate:short)|%(taggername) a|%(subject)%0A%0A%(body)"
                WORKING_DIRECTORY ${CMAKE_SOURCE_DIR}
                OUTPUT_VARIABLE TAG_INFO
                ENCODING UTF-8
            )
            list(APPEND HISTORY ${TAG_INFO})
        endif()
    ENDFOREACH()

    list(JOIN HISTORY "\n" HISTORY)
    set(HISTORY ${HISTORY} PARENT_SCOPE)

ENDFUNCTION()
